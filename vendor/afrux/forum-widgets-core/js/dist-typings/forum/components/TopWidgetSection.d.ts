import * as Mithril from 'mithril';
import Component from 'flarum/common/Component';
export default class TopWidgetSection extends Component {
    view(): Mithril.Children;
}
